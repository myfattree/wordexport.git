package com.fattree.file.word.service;

import javax.servlet.http.HttpServletResponse;

public interface ExportWordService {
    void exportDocWord(HttpServletResponse response, String extName);
}
