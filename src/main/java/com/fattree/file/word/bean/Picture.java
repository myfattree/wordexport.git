package com.fattree.file.word.bean;

import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author 肥树
 * @Description: TODO (  )
 * @Date 2021/12/14 4:35 PM
 * @Version V1.0
 */
@Data
public class Picture {
    /**
     * word 中的文件名称
     */
    String pictureName;
    /**
     * 存储在磁盘中的文件名称（带路径），为了方便生成 word 文档
     */
    String fileName;
    /**
     * 图片输入流
     */
    ByteArrayOutputStream fos;
}