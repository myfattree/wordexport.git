package com.fattree.file.word.bean;

import lombok.Data;

@Data
public class Score {
    String sort;
    String subject;
    String score;
}
