package com.fattree.file.word.api;

import com.fattree.file.word.service.ExportWordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/file")
public class ExportWordAction {
    @Autowired
    private ExportWordService exportWordService;

    @RequestMapping(value = "/word/export", method = RequestMethod.GET)
    public void exportDocWord(HttpServletRequest request,
                              HttpServletResponse response,
                              @RequestParam(required = false) String extName) {
        try {
            exportWordService.exportDocWord(response, extName);
        } catch (Exception e) {

        }
    }
}
