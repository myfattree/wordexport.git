package com.fattree.file.word.utils;

import com.fattree.file.word.bean.Word;

/**
 * @author 肥树
 * @Description: TODO (  )
 * @Date 2021/12/14 11:31 AM
 * @Version V1.0
 */
public class MainUtils {
    public static void main(String[] args) {
        try {
            //生成文件
            Word doc = DataUtils.getWord("./src/main/resources/docs/test.doc");
            Word docx = DataUtils.getWord("./src/main/resources/docs/test.docx");
            WordUtils.createWordDoc(doc);
            WordUtils.createWordDoc(docx);
            //获取文件内容
            Word doc1 = DataUtils.getWord("./src/main/resources/docs/考核登记表.doc");
            Word docx1 = DataUtils.getWord("./src/main/resources/docs/考核登记表.docx");
            WordUtils.readWordDocTable(doc1);
            WordUtils.readWordDocTable(docx1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}