package com.fattree.file.word.utils;

import com.fattree.file.word.bean.Picture;
import com.fattree.file.word.bean.Report;
import com.fattree.file.word.bean.Score;
import com.fattree.file.word.bean.Word;

import java.io.*;
import java.util.*;

public class DataUtils {

    public static Report getData(String fileExt) {
        int capacity = 10;

        Report report = new Report();
        report.setUserName("张三丰");
        report.setSex("男");
        report.setNation("蒙古族");
        if ("doc".equals(fileExt)) {
            report.setHeader((String) getImage("./src/main/resources/static/img/ChoutiWallPaper-2021-04-15.jpeg", 1));
            report.setImage((String) getImage("./src/main/resources/static/img/ChoutiWallPaper-2021-10-22.jpeg", 1));
        } else {
            Map<String, Picture> pictureMap = new HashMap<>();
            Picture header = (Picture) getImage("./src/main/resources/static/img/ChoutiWallPaper-2021-04-15.jpeg", 2);
            Picture image = (Picture) getImage("./src/main/resources/static/img/ChoutiWallPaper-2021-10-22.jpeg", 2);
            report.setHeader(header.getPictureName());
            report.setImage(image.getPictureName());
            pictureMap.put(report.getHeader(), header);
            pictureMap.put(report.getImage(), image);
            report.setPictureMap(pictureMap);
        }

        List<Score> scoreList = new ArrayList<>(capacity);

        Score score = new Score();
        score.setSort("1");
        score.setSubject("语文");
        score.setScore("90");
        scoreList.add(0, score);

        score = new Score();
        score.setSort("2");
        score.setSubject("数学");
        score.setScore("120");
        scoreList.add(1, score);

        score = new Score();
        score.setSort("3");
        score.setSubject("英语");
        score.setScore("110");
        scoreList.add(2, score);

        score = new Score();
        score.setSort("4");
        score.setSubject("物理");
        score.setScore("110");
        scoreList.add(3, score);

        score = new Score();
        score.setSort("5");
        score.setSubject("化学");
        score.setScore("110");
        scoreList.add(4, score);

        score = new Score();
        score.setSort("6");
        score.setSubject("生物");
        score.setScore("110");
        scoreList.add(5, score);

        score = new Score();
        score.setSort("7");
        score.setSubject("政治");
        score.setScore("110");
        scoreList.add(6, score);

        score = new Score();
        score.setSort("8");
        score.setSubject("历史");
        score.setScore("110");
        scoreList.add(7, score);

        score = new Score();
        score.setSort("9");
        score.setSubject("地理");
        score.setScore("110");
        scoreList.add(8, score);

        report.setScoreList(scoreList);
        report.setTotalScore("320");
        report.setSelfJudge("勤学好问，乐于助人");
        return report;
    }

    public static Word getWord(String docName) {
        String extName = docName.substring(docName.lastIndexOf(".") + 1);
        if (extName == null || "".equals(extName.trim())) {
            extName = "doc";
        }
        Word word = new Word();
        word.setFileExt(extName);
        word.setTemplatePath("/templates");
        if ("doc".equals(extName.toLowerCase())) {
            word.setTemplateName("doc.ftl");
        } else if ("docx".equals(extName.toLowerCase())) {
            word.setTemplateName("docx.ftl");
            word.setTemplateZipName("docx.zip");
        }
        word.setDocName(docName);
        word.setData(DataUtils.getData(word.getFileExt()));
        return word;
    }

    public static String getImageBase64(String filename) {
        try {
            InputStream in = new FileInputStream(filename);
            byte[] data = new byte[in.available()];
            in.read(data);
            in.close();
            String base64 = Base64.getEncoder().encodeToString(data);
            //System.out.println(base64);
            return Base64.getEncoder().encodeToString(data);
        } catch (Exception e) {
            e.printStackTrace();
            return "图片未找到";
        }
    }

    public static Object getImage(String filename, int type) {
        try {
            if (1 == type) {
                return getImageBase64(filename);
            } else if (2 == type) {
                Picture picture = new Picture();
                int start = filename.lastIndexOf("/");
                int end = filename.lastIndexOf(".");
                InputStream in = new FileInputStream(filename);
                byte[] data = new byte[in.available()];
                in.read(data);
                in.close();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bos.write(data);
                bos.close();
                picture.setPictureName(filename.substring(start + 1, end));
                picture.setFileName(filename.substring(start + 1));
                picture.setFos(bos);
                return picture;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "图片未找到";
        }
        return null;
    }
}
