package com.fattree.file.pdf;

import com.aspose.words.Document;
import com.aspose.words.SaveFormat;
import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.PdfPageBase;
import com.spire.pdf.exporting.PdfImageInfo;
import com.spire.pdf.general.find.PdfTextFind;
import com.spire.pdf.general.find.PdfTextFindCollection;
import com.spire.pdf.graphics.PdfBrushes;
import com.spire.pdf.graphics.PdfRGBColor;
import com.spire.pdf.graphics.PdfSolidBrush;
import com.spire.pdf.graphics.PdfTrueTypeFont;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * word document to pdf transfer test
 *
 * @author <a href="https://gitee.com/myfattree" target="_blank">肥树<a/>
 * @version 1.0
 * @mail wangjiaxiu@xiyuntu.com
 * @date 2022/3/7 10:04 AM
 * @desc 使用 aspose-words 将 doc/docx 转换为 pdf 文件。因为没有使用 license ，所以生成的 pdf 是带有文本水印和图片水印的。
 * 使用 spire.pdf 将生成 pdf 中的文本水印进行替换/覆盖，然后删除掉图片水印，该步骤会将自己添加的图片水印一起删掉。
 * 所以如果需要加上自己的图片水印，那么在删除掉 aspose-words 自带水印后，使用 spire.pdf 添加自己的图片水印即可。
 */
public class Word2PdfMain {
    public static void main(String[] args) {
        try {
            long start = System.currentTimeMillis();
            File file = new File("");
            String basePath = file.getAbsolutePath();
            String docFilePath = basePath + "/src/main/resources/docs/考核登记表-加水印.docx";
            String pdfFilePath = basePath + "/src/main/resources/out/考核登记表.pdf";
            File pdfFile = new File(pdfFilePath);
            FileOutputStream os = new FileOutputStream(pdfFile);
            Document document = new Document(docFilePath);
            document.save(os, SaveFormat.PDF);
            os.close();

            //删除（覆盖）页眉页脚的文本水印
            String noAdditionPdfFilePath = basePath + "/src/main/resources/out/考核登记表-无水印.pdf";
            List<String> findTextList = new ArrayList<>();
            findTextList.add("Evaluation Only. Created with Aspose.Words. Copyright 2003-2022 Aspose Pty Ltd.");
            findTextList.add("Created with an evaluation copy of Aspose.Words. To discover the full versions of our APIs");
            findTextList.add("please visit: https://products.aspose.com/words/");
            //加载示例PDF文档
            PdfDocument pdf = new PdfDocument();
            pdf.loadFromFile(pdfFilePath);

            //定义一个int型变量
            int index = 0;
            //遍历文档每一页
            for (int i = 0; i < pdf.getPages().getCount(); i++) {
                //获取所有页面
                PdfPageBase page = pdf.getPages().get(i);
                for (String findText : findTextList) {
                    //查找指定文本
                    PdfTextFindCollection textFindCollection = page.findText(findText, false);
                    //创建画刷、字体
                    PdfSolidBrush brush = new PdfSolidBrush(new PdfRGBColor(Color.WHITE));
                    PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("宋体", Font.PLAIN, 9), true);
                    //用新的文本字符替换原有文本
                    Rectangle2D rec;
                    for (PdfTextFind find : textFindCollection.getFinds()) {
                        rec = find.getBounds();
                        rec.setRect(rec.getX(), rec.getY(), rec.getWidth(), 14);
                        page.getCanvas().drawRectangle(PdfBrushes.getWhite(), rec);
                        page.getCanvas().drawString("", font, brush, rec);
                    }
                }
                //导出所有图片
                //使用extractImages方法获取指定页上图片
                if (page.extractImages() != null) {
                    for (BufferedImage image : page.extractImages()) {
                        String imgFilePath = basePath + "/src/main/resources/out/" + String.format("Image_%d.png", index++);
                        //指定输出文件路径及名称
                        File output = new File(imgFilePath);
                        //将图片保存为PNG格式文件
                        ImageIO.write(image, "PNG", output);
                    }
                }
                // 删除所有图片
                //获取页面上的图片信息
                PdfImageInfo[] imageInfo = page.getImagesInfo();
                //遍历每一个图片
                for (int j = imageInfo.length; j > 0; j--) {
                    //通过图片的索引删除图片
                    page.deleteImage(j - 1);
                }
            }
            //保存文档
            pdf.saveToFile(noAdditionPdfFilePath, FileFormat.PDF);
            pdf.close();
            long end = System.currentTimeMillis();
            System.out.println("use time : " + (end - start) + " ms");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}