package com.fattree.apachepoi.word.utils;

/**
 * @author 肥树 wangjiaxiu248@163.com
 * @Package com.fattree.apachepoi.word.utils
 * @Description: TODO (  )
 * @Date 2022/1/24 6:07 PM
 * @Version V1.0
 */
public class ConsistentUtils {
    final public static int INT_ZERO = 0;
    final public static int INT_ONE = 1;
    final public static String STR_ZERO = "0";
    final public static String STR_ONE = "1";
}