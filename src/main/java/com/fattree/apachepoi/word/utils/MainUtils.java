package com.fattree.apachepoi.word.utils;

import com.fattree.apachepoi.word.bean.Watermark;
import org.apache.poi.xwpf.usermodel.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 肥树 wangjiaxiu248@163.com
 * @Package com.fattree.apachepoi.word.utils
 * @Description: TODO (  )
 * @Date 2022/1/24 12:26 PM
 * @Version V1.0
 */
public class MainUtils {
    public static void main(String[] args) {
        try {
            String path = "/Users/wangjiaxiu/gitee/wordexport/src/main/resources/docs/";
            String fileName = path + "考核登记表-poi测试.docx";
            String newFileName = path + "考核登记表-poi测试-new.docx";
            XWPFDocument document = ApachePOIUtils.getDocument(fileName);
            //添加水印
            ApachePOIUtils.addWatermark(document, getWatermark());
            //模板参数替换
            List<XWPFParagraph> paragraphList = ApachePOIUtils.getParagraphs(document);
            for (XWPFParagraph paragraph : paragraphList) {
                ApachePOIUtils.replaceTextForParagraphNew(paragraph, getDataMap());
            }
            //导出新文件
            ApachePOIUtils.writeDocument(document, newFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Watermark getWatermark() {
        Watermark watermark = new Watermark();
        watermark.setType(ConsistentUtils.INT_ONE);
        watermark.setValue("肥树0318");
        watermark.setFontSize(100);
        return watermark;
    }

    private static Map<String, Object> getDataMap() {
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("year", "2021");
        dataMap.put("date", "2021-12-29");
        dataMap.put("name", "肥树");
        dataMap.put("gender", "其他");
        dataMap.put("political", "群众");
        dataMap.put("nation", "汉族");
        dataMap.put("org", "兰州");
        dataMap.put("station", "工程师");
        dataMap.put("work", "                 员工大会发言稿\n" +
                "\n" +
                "        各位同事，下午好。\n" +
                "        今天我们齐聚一堂，我很荣幸能够作为优秀员工代表在这个正式的场合发言。\n" +
                "        首先，我代表获得荣誉称号的各位优秀员工，向支持我们、帮助我们、信任我们的所有同事表示衷心的感谢。\n" +
                "        刚刚过去的2021年，是紧张忙碌的一年，是充实成长的一年，也是守望相助的一年。在疫情防控常态化的这一年里，公司保持了稳步的发展，并不断开拓新的业务；很多优秀的新同事加入了我们，我们的团队也在不断发展壮大。这里面的每一分进步都离不开大家的共同努力。\n" +
                "        相信有很多同事在这一年里有着或多或少的感触和难忘的经历。可能是上班路上拥挤的车厢，可能是办公室外绚丽的晚霞，也可能是办公楼凌晨四点的灯光。每个人都在为了公司这个大集体和各自的小家庭努力地付出着。我们勤劳务实、团结奋进，我们不在光里，但依然是生活的勇者。\n" +
                "        新年伊始，百事待兴，作为优秀员工，我们会戒骄戒躁、再接再厉、精益求精，发扬团队精神，和所有同事一起取得更大的进步。\n" +
                "        同时也祝愿各位同事，有强健的体魄、有不灭的热情、有坚定的信念、所有付出终有回报！\n" +
                "        谢谢大家！\n" +
                "\n" +
                "                                         肥树，2022.01.17");
        dataMap.put("grade", "三年四班");
        dataMap.put("teacher", "肥树的老师");
        dataMap.put("score", "98.5");
        dataMap.put("level", "A");
        dataMap.put("leaderOpinion", "非常好\n认真刻苦");
        dataMap.put("leaderSign", "肥树的领导");
        dataMap.put("leaderSignDate", "2021年12月30日");
        dataMap.put("communicate", "是");
        dataMap.put("agree", "是");
        dataMap.put("selfOpinion", "还可以\n自我感觉良好");
        dataMap.put("selfSign", "肥树自己");
        dataMap.put("selfSignDate", "2021年12月31日");
        dataMap.put("company", "甘肃兰州公司");
        dataMap.put("total", "365.8");
        dataMap.put("evaluate", "我已出舱，感觉良好");
        dataMap.put("header", "./src/main/resources/static/img/ChoutiWallPaper-2021-04-15.jpeg");
        dataMap.put("picture", "./src/main/resources/static/img/ChoutiWallPaper-2021-10-22.jpeg");
        dataMap.put("link", "http://deepoove.com/poi-tl/apache-poi-guide.html");
        return dataMap;
    }
}