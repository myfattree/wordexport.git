package com.fattree.apachepoi.word.bean;

/**
 * Created by wangjiaxiu on 2022/1/26.
 */
public enum TParamType {
    /**
     * 系统
     */
    SYSTEM("$", "system"),
    /**
     * 默认类型，文本
     */
    DEFAULT("t#", "text"),
    /**
     * 文本类型
     */
    TEXT("t#", "text"),
    /**
     * 超链接
     */
    ANCHOR("a#", "anchor"),
    /**
     * 单图片
     */
    IMAGE("img#", "image"),
    /**
     * 多图片
     */
    IMAGES("imgs#", "images"),
    /**
     * 列表
     */
    LIST("list#", "list");

    private String tag = "";
    private String name = "";

    private TParamType(String tag, String name) {
        this.tag = tag;
        this.name = name;
    }

    public String getTag() {
        return this.tag;
    }

    public String getName() {
        return this.name;
    }
}
