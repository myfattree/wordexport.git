package com.fattree.apachepoi.word.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author 肥树 wangjiaxiu248@163.com
 * @Package com.fattree.apachepoi.word.bean
 * @Description: TODO (  )
 * @Date 2022/1/24 5:57 PM
 * @Version V1.0
 */
@Data
public class Watermark implements Serializable {
    /**
     * 水印类型， 0为无水印； 1为文字水印
     */
    private int type = 0;
    /**
     * 文字水印的文字，当type为1时此字段必选
     */
    private String value = "严禁复制";
    /**
     * 水印的透明度，非必选，有默认值
     * "rgba( 192, 192, 192, 0.6 )"
     */
    private String fillColor = "#d8d8d8";

    /**
     * 长度/间距单位,默认 pt
     */
    private String unit = "px";
    /**
     * 水印的字体，非必选，有默认值
     * "bold 20px Serif"
     */
    private String fontName = "宋体";
    /**
     * 水印的字体大小
     */
    private float fontSize = 20;

    /**
     * 水印的旋转度，非必选，有默认值
     */
    private float rotate = 315;
    /**
     * 水印水平间距，非必选，有默认值
     * 一个字平均长度，用于：计算文本占用的长度（文本总个数*单字长度）
     */
    private float horizontal = 10;
    /**
     * 水印垂直间距，非必选，有默认值
     * 与顶部的间距
     */
    private float vertical = 0;
}