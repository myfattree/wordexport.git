package com.fattree.apachepoi.word.bean;

import lombok.Data;

/**
 * @author 肥树 wangjiaxiu248@163.com
 * @Package com.fattree.apachepoi.word.bean
 * @Description: TODO ( template's parameters define )
 * @Date 2022/1/26 5:46 PM
 * @Version V1.0
 */
@Data
public class TParam {
    /**
     * 参数名称
     */
    private String paramName;
    /**
     * 参数类型
     */
    private TParamType paramType = TParamType.DEFAULT;
    /**
     * 参数为容器对象时匹配前缀
     */
    private String prefix = "";

    private String children = "";
}