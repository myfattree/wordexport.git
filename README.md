# freemarker 模板
咨询了两位同事，其他项目上用到了word生成和导出，结合资料和自己的试验，技术上是可行的。主要是使用了 freemarker 模板填充占位符的方式来生成word文档，支持跨平台。

## 实现步骤
### 1 doc文件生成
#### 1.1 模板制作
- 制作doc文件模板的详细步骤可参考 [Java使用FreeMarker自动生成Word文档（带图片和表单）](https://blog.csdn.net/weixin_44516305/article/details/88049964)这篇文章的 [3 创建Word模板](https://blog.csdn.net/weixin_44516305/article/details/88049964#3_Word_13)这部分内容
- 简单来说只需要将doc文件直接另存为xml，修改xml内容使之符合freemarker解析规范，并将xml扩展名改为ftl，并将ftl文件放到代码工程的静态资源路径下

#### 1.2 工程创建
- 1.2.1 创建springboot工程
- 1.2.2 在pom.xml中添加freemarker依赖及springboot相关依赖
- 1.2.3 在src/main/resources下创建文件目录templates
- 1.2.4 将前面制作好的模板放到templates目录下
- 1.2.5 开始编码、测试

### 2 docx文件生成
#### 2.1 模板制作
- 制作docx文件模板的详细步骤可参考 [java利用Freemarker模板生成docx格式的word文档](https://my.oschina.net/u/3737136/blog/2958421?tdsourcetag=s_pcqq_aiomsg)这篇文章
- 生成内容为表格文本的docx文件的详细步骤可参考 [java利用Freemarker模板生成docx格式的word文档（全过程）](https://www.cnblogs.com/ssyh/p/12494626.html)这篇文章。这篇文章参考了上面这篇文章，所以实现的步骤差不多一样。
- 简单来说docx模板制作主要分为三步：
- [x] 1 将docx扩展名改为zip
- [x] 2 打开zip并拷贝并另存为压缩包中word/document.xml文件
- [x] 3 修改拷贝出的xml内容使之符合freemarker解析规范，并将xml扩展名改为ftl
- [x] 4 将document.ftl与zip包放到代码工程的静态资源路径下
- docx模板使用注意事项：
- [x] 1 zip包为原docx文件的zip包，不要将修改后的document.xml覆盖替换原文件
- [x] 2 一个docx文件只对应一个document.ftl，所以有多个docx文件时，注意模板名称不要重复

#### 2.2 工程创建
- 2.2.1 创建springboot工程
- 2.2.2 在pom.xml中添加freemarker依赖及springboot相关依赖
- 2.2.3 在src/main/resources下创建文件目录templates
- 2.2.4 将前面制作好的模板document.ftl与zip包放到templates目录下
- 2.2.5 开始编码、测试

### 3 示例代码
- 在线浏览：[肥树 / wordexport](https://gitee.com/myfattree/wordexport)
- 克隆下载：[https://gitee.com/myfattree/wordexport.git](https://gitee.com/myfattree/wordexport.git)

## 事项说明
- 目前仅测试了扩展名为doc及docx且内容为表格文本的word文件的生成，没有对图片、超链接等进行测试验证
- 使用freemarker模板生成word文件，前提是需要制作好模板，根据使用场景不同来填充不同模板生成word文档
- 不同与其他项目中将生成的word文件先上传到mongodb再根据fileId下载文件的方式，我自己编写的代码是访问时直接下载，并没有将word文档存储到mongodb。具体采用哪种方式可以根据实际需求来进行选择

### 关于图片导出
- doc 文件导出图片比较简单，只需要在模板的需要展示图片的位置设置占位符，将需要导出的图片转为 base64 编码即可替换。此方法已经过验证，是可行的，gitee 示例代码已经更新。
- docx 文件导出图片比较麻烦些，首先将模板中需要展示图片的位置以及引用id都设置占位符（此占位符一般会替换为导出文件的文件名称），在导出图片时将需要导出的图片文件压缩到模板 zip 文件 word/media 目录中，同时用图片文件的名称（不包含扩展名）替换占位符，再将 zip 中 word/_rels/document.xml.rels 文件里追加导出图片的引用。此方法已经过验证，是可行的，gitee 示例代码已经更新。

## 参考资料
- 其他项目导出为word的代码，同事提供
- [java导出word的5种方式](https://www.cnblogs.com/ziwuxian/p/8985678.html)，ziwuxian
- [Java 基于freemarker实现Word模板导出并下载](https://blog.csdn.net/Mr_hwt_123/article/details/105708270)，Mr_hwt_123
- [Java使用FreeMarker自动生成Word文档（带图片和表单）](https://blog.csdn.net/weixin_44516305/article/details/88049964)，JavaBigData1024
- [java利用Freemarker模板生成docx格式的word文档](https://my.oschina.net/u/3737136/blog/2958421?tdsourcetag=s_pcqq_aiomsg)，lemonLove
- [java利用Freemarker模板生成docx格式的word文档（全过程）](https://www.cnblogs.com/ssyh/p/12494626.html)，旁光

# apache-poi

## XWPFDocument

| Modifier and Type                                            | Field and Description | 备注                 |
| ------------------------------------------------------------ | --------------------- | -------------------- |
| protected java.util.List<IBodyElement>                       | bodyElements          | 迭代器（段落和表格） |
| protected java.util.List<XWPFChart>                          | charts                | 图表                 |
| protected java.util.List<XWPFComment>                        | comments              | 批注                 |
| protected java.util.List<XWPFSDT>                            | contentControls       | 内容控制             |
| protected XWPFEndnotes                                       | endnotes              | 尾注                 |
| protected java.util.List<XWPFFooter>                         | footers               | 页脚                 |
| protected XWPFFootnotes                                      | footnotes             | 脚注                 |
| protected java.util.List<XWPFHeader>                         | headers               | 页眉                 |
| protected java.util.List<XWPFHyperlink>                      | hyperlinks            | 超链接               |
| protected XWPFNumbering                                      | numbering             | 编号                 |
| protected java.util.Map<java.lang.Long,java.util.List<XWPFPictureData>> | packagePictures       | 图片                 |
| protected java.util.List<XWPFParagraph>                      | paragraphs            | 段落（文字）         |
| protected java.util.List<XWPFPictureData>                    | pictures              | 图片                 |
| protected XWPFStyles                                         | styles                | 字体样式             |
| protected java.util.List<XWPFTable>                          | tables                | 表格                 |

## 版本
使用了两个版本的 apache-poi
- 3.12 : 使用该版本请注释掉 ```com/fattree/apachepoi/word/utils``` 中的 ```ApachePOIUtils.java``` 和 ```MainUtils.java``` 这两个类
- 4.1.2: 使用该版本请注释掉 ```com/fattree/apachepoi/word/utils``` 中的 ```ApachePOIUtils312.java``` 和 ```MainUtils312.java``` 这两个类

## 参考资料
- [Apache POI Word(docx) 入门示例教程](http://deepoove.com/poi-tl/apache-poi-guide.html)
- [poi实现word模板数据写入支持doc/docx](https://blog.csdn.net/qq_15550525/article/details/108280301)
- [Java使用poi实现Word添加水印（仅支持后缀为.docx格式）](https://www.cnblogs.com/shift25/p/15793673.html)
- [如何使用Apache POI从.docx文件中检索水印文本？](https://cloud.tencent.com/developer/ask/sof/259884)
- [java使用poi给word添加多个水印](https://blog.csdn.net/z172989496/article/details/110492356)
- [Add watermark in word document using Apache POI](https://stackoverflow.com/questions/48248276/add-watermark-in-word-document-using-apache-poi)
- [JAVA操作Word合并、替换占位符、Word插入富文本、生成水印](https://www.jianshu.com/p/64819dc4459a)
- [Apache POI Word(docx) 入门示例教程](http://deepoove.com/poi-tl/apache-poi-guide.html)

# easy-poi
## 参考资料

# poi-tl
## 关于 apache-poi
- poi-tl（poi template language）是Word模板引擎，使用Word模板和数据创建很棒的Word文档。
- poi-tl 是一个基于Apache POI的Word模板引擎，也是一个免费开源的Java类库

## 参考资料
- [Poi-tl Documentation](http://deepoove.com/poi-tl/)
- []()